# NOTAS SOBRE COMPILAÇÃO, SETUP E ADMINISTRAÇÃO DE AGENTES USANDO RMA DE JADE
###### **By Italo Ramon Campos e Marcos Sousa** 

<br>

### Estrutura básica de um sistema multiagentes no JADE
Um sistema multiagentes no JADE possui uma organização básica definida que visa a melhor gestão do sistema. Um sistema multiagentes no JADE é composto por:

+ __Plataforma (Platform):__ espaço que fornece um ambiente (contêineres e agentes) para a execução de tarefas dos agentes.
    + **Contêiner (Container):** roda sobre um _host_ e contém agentes.
        + **Agente (Agent):** entidade que possui e executa comportamentos e se comunica através do sistema.
    + **Contêiner Principal (Main Container):** É o primeiro a ser executado na plataforma em sua inicialização e toda a plataforma deve ter um. Os outros *containers*
    da plataforma devem ser registrados nele. Possui dois agentes especiais com tarefas específicas.
        + **AMS:** é a autoridade na plataforma. É o único agente a executar comandos de gerenciamento da plataforma (p.e. iniciar, encerrar agentes, desligar
        a plataforma). Outros agentes podem requisitar serviços deste agente.
        + **DF:** formece _yellow pages_ que são espaços para os agentes da mesma plataforma publicarem seus serviços e encontrarem serviços de outros agentes.

Contêiners e plataformas em diferentes contextos (especialmente sistemas operacionais e _hosts_) podem compor um único sistema multiagentes (sistema multiagentes
distribuído), desde que estejam interconectados e possam trocar mensagens.

<br>

### Comunicação entre agentes
A comunicação entre agentes no JADE é feita através de mensagens no padrão ACL (Agent Communications Language) definido pela FIPA (Foundation for Intelligent
Physical Agents). As mensagens ACL têm os seguintes campos em sua estrutura básica:

+ **Emissor (Sender):** o agente que envia a mensagem.
+ **Receptor (Receivers):** o agente que deve receber a mensagem (o nome do agente deve ser no formato global, descrito abaixo).
+ **Ato comunicativo (_Communicative act_, também chamado _Performative_):** o tipo de comunicação que está sendo estabelecida. Existem duas classes de 
_communicative act_: INFORM message e REQUEST message.
    + INFORM message: o emissor deseja alertar o(s) receptor(es) sobre algo.
    + REQUEST message: o emissor deseja que o(s) receptor(es) executem uma tarefa.
+ **Conteúdo (Content):** o conteúdo da mensagem (a tarefa que o agente deverá executar ou o fato que será alertado ao agente receptor.

Os nomes dos agentes no sistema possuem uma especificação própria. Essa especificação é chamada de _nome global_ do agente e é na forma
``` shell
<agents_local_name>@<platform_name>
```
Assim, todos agentes da mesma plataforma precisam ter um nome único, mas nada impede que agentes em diferentes plataformas tenham o mesmo nome, já que seus nomes globais
são diferentes.

##### MTP e HTTP
A comunicação entre agentes em diferentes plataformas é implementada através do protocolo MTP (Message Transport Protocol). MTP em essência não é um protocolo de rede, 
mas um módulo de comunicação. Para realizar a comunicação entre plataformas distribuídas o MTP pode utilizar os protocolos HTTP, IIOP e SMTP (não suportado pelo JADE).
Por padrão, o MTP vem instalado apenas em contêineres principais no JADE RAM e utiliza o protocolo HTTP.

<br>

### Ferramentas do JADE RAM (Remote Agent Management GUI)
O JADE RMA fornece algumas ferramentas úteis para o gerenciamento de sistemas multiagentes. Abaixo segue uma pequena lista com alguns recursos.
+ **Dummy Agent:** agente genérico do JADE que provê ferramentas para o controle dos outros agentes do contêiner.
+ **Sniffer Agent:** inspeciona a troca de mensagens entre agentes.
+ **Debugger Agent:** inspeciona características internas dos agentes (p.e. as tarefas que um agente está executando em determinado momento).
+ **Log Manager Agent:** permite a troca dos _log levels_ das classes em tempo de execução.

<br>

### Utilizando a ferramenta RAM
##### Compilando arquivos .java com dependências das classes JADE

``` shell
# Windows
javac -classpath lib\jade.jar -d pasta_destino caminho\do\pacote\classe.java

#Linux
javac -classpath lib/jade.jar -d pasta_destino caminho/do/pacote/classe.java
```

Onde:
+ **lib/jade.jar** é o caminho das classes JADE (biblioteca) das quais as classes a serem compiladas dependem;
+ **pasta_destino** é um diretório onde serão colocados os arquivos compilados (.class);
+ **caminho/do/pacote/classe.java** é o caminho incluindo o pacote (package) da classe Java a ser compilada.


>>>

**Observe:**  
+ Os novos agentes implementados (com o JADE) nada mais são o que classes que herdam características das classes pré-implementadas pelo JADE (p.e. `jade.core.Agent`, 
`jade.core.behaviours.Behaviour`). Daí a necessidade de, ao compilar qualquer nova classe (agente), incluir a biblioteca JADE (lib/jade.jar) no _classpath_.  
+ Considerando que a biblioteca JADE não está inclusa em CLASSPATH do sistema, será necessário invocá-la a cada compilação ou execução do RMA. Caso o JADE esteja
incluso nas variáveis de ambiente do sistema ignore a chamada do JADE `-classpath lib/jade.jar`.

>>>


##### Executando o JADE RAM GUI
O comando básico para iniciar uma nova plataforma com o JADE RAM GUI (invocando explicitamente o _classpath_ do JADE) é na forma:
``` shell
java -cp lib/jade.jar:other_classes jade.Boot -options
```

Onde:
+ **other_classe** é o _classpath_ (caminho para suas classes serem usadas juntamente com as do JADE).
+ **options** são opções que podem ser usadas com o RAM.

Abaixo há uma lista de opções que podem ser usadas com o RAM:
+ **-gui**: inicia a interface gráfica java.swing com a plataforma.
+ **-local-port [num-port]**: inicia uma nova plataforma 'escutando' na porta _[num-port]_.
+ **-local-host [url-or-ip-address]**: inicia uma nova plataforma no host _[url-or-ip-address]_.
+ **-platform-name [platform-name]**: inicia uma nova plataforma com o nome _[platform-name]_.
+ **-agents [agent1:classpath;agent2:classpath]**: inicia uma nova plataforma incluindo os agentes [agent1:classpath;agent2:classpath]. Vários agentes podem ser
iniciados separando-os por ';'.
+ **-container**: um novo container (container periférico) é iniciado. Esse comando diferencia-se dos anteriores por requerer que uma plataforma contendo um _main-container_
já esteja iniciada. As sub-opções abaixo podem ser usadas com esta opção:
    + **-host [host]**: define o _host_ onde uma plataforma já está iniciada (o novo _container_ é iniciado lá). Se essa opção não for usada, por padrão o container
    procurará uma plataforma em _localhost_.
    + **-port [num_port]**: define a porta onde uma plataforma está 'escutando'. Se essa opção não for usada, por padrão a porta procurada será 1099.


##### Instanciando novos agentes com RAM
Para iniciar o RAM do JADE instanciando novos agentes executamos o comando abaixo. Nestes, invocamos explicitamente o _classpath_ do JADE:
``` shell
# Windows
java -cp lib\jade.jar;classess_compiladas jade.Boot -gui -agents novo_agente:caminho.do.pacote

#Linux
java -cp lib/jade.jar:classess_compiladas jade.Boot -gui -agents novo_agente:caminho.do.pacote
```

Onde:
+ **classes_compiladas** é o caminho para as classes compiladas previamente (que implementam os agentes)
+ **novo_agente** é o nome do novo agente a ser instanciado juntamente com o RAM
+ **caminho.do.pacote** é a instância do pacote (se houver) que contém a classe principal do agente

<br>

### Comportamentos
São instâncias da classe jade.core.Behaviours e suas filhas. A tabela abaixo descreve alguns dos principais métodos da classe Behaviours.

| __Método__ | __Argumentos__ | __Retorno__ | __Descrição__ | __Obrigatório__ |
| ---------- | -------------- | ----------- | ------------- | --------------- |
| action | - | _void_ | Define a ação que será executada no comportamento. | Sim |
| done | - | _Boolean_ | Executado sempre depois do método action. Deve retornar __true__ se o comportamento terminou ou __false__ caso contrário. | Sim |
| block | _long: delay_ | _void_ | Depoi de executar o método __action__ coloca o comportamento no estado de _bloqueado_ por __delay__ milisegundos. | Não |
| onEnd | - | _int_ | Define ações que são realizadas no momento em que o agente é finalizado. Deve retornar um inteiro para indicar se o agente foi encerrado corretamente (zero, neste caso). | Não |
| takeDown | - | _void_ | Define ações que são realizadas no momento em que o agente é finalizado. | Não |  

Toda instância da classe Behaviours possui uma variável chamada __myAgent__ que referencia o objeto do agente que executa este comportamento.
Para invocar essa variável é necessário uma chamada
``` java
super(agente)
```
no construtor da classe que implementa o comportamento, onde _agente_ é um objeto do agente que executa o referido comportamento.
Para adicionar um comportamento (objeto da classe Behaviours e filhas) a um agente utilize:
``` java
addBehaviour(new ClasseQueImplementaComportamento(this, "others args");
```

#### Tipos de comportamento
O JADE implementa comportamentos pré-definidos que são classificados em quatro tipos. Abaixo segue um breve resumo sobre eles.

#### One-shot Behaviour
Características:  
+ Disponível na classe  _jade.core.behaviours.OneShotBehaviour_;
+ Executam uma única vez;
+ Método __done()__ não é implementado;.

#### Cyclic Behaviour
Características:  
+ Disponível na classe _jade.core.behaviours.CyclicBehaviour_;
+ Executam indefinidas vezes (a cada chamada do escalonador de comportamentos);
+ Método __done()__ não é implementado;
+ Utiliza o método __stop()__ para parar sua execução.

#### Comportamentos Temporais
São classificados em dois subtipos:

##### __1. WakerBehaviour__:
+ Disponível na classe _jade.core.behaviours.WakerBehaviour_;
+ Aguarda um tempo inicial (em milisegundos) para ser executado;
+ É executado uma única vez;
+ Método __done()__ não é implementado;
+ Método __action()__ não é implementado;
+ Implementa sua ação no método __onWake()__ ou __handleElapsedTimeout()__;
+ Necessita de um argumento do tipo _long_ em sua instanciação, que representa o número de milisegundos que o comportamento deve aguardar
até ser executado.

##### __2. TickerBehaviour__:
+ Disponível na classe _jade.core.behaviours.TickerBehaviour_;
+ Aguarda um tempo inicial (em milisegundos) para ser executado;
+ É executado ciclicamente;
+ Método __done()__ não é implementado;
+ Método __action()__ não é implementado;
+ Implementa sua ação no método __ontick()__;
+ Necessita de um argumento do tipo _long_ em sua instanciação, que representa o número de milisegundos que o comportamento deve aguardar
até ser executado;
+ Pode ser reiniciado com o método __reset()__;
+ Pode ser terminado comos métodos __stop()__ ou __removeBehaviour()__;
+ Possui um método chamado __getTickCount()__ que retorna o número (inteiro) do ciclo que o comportamento está executanto.


#### Comportamentos Compostos
São basicamente comportamentos que implementam vários comportamentos (chamados subcomportamentos) em sua execução. São classificados em três
subtipos:

##### __1. SequencialBehaviour__:
+ Executa seus subcomportamentos de maneira sequancial, isto é, um após o outro;
+ A ordem em que os subcomportamentos são adicionados indica a ordem em que serão executados;
+ Método __done()__ não é implementado;
+ Método __action()__ não é implementado;
+ Adiciona sub comportamentos com o método __addSubBehaviour(Behaviour)__, onde _Behaviour_ é um objeto da classe Behaviour ou filhas;

##### __2. ParallelBehaviour__:
+ Executa seus subcomportamentos de maneira paralela, isto é, todos os seus subcomportamentos são iniciados ao mesmo tempo;
+ Método __done()__ não é implementado;
+ Método __action()__ não é implementado;
+ Finaliza em três casos diferentes:
    + Quando todos os seus subcomportamentos terminam (ParallelBehaviour._WHEN\_ALL_)
    + Quando qualquer um dos seus subcomportamentos termina (ParallelBehaviour._WHEN\_ANY_)
    + Quando um determinado número N de subcomportamentos terminam (Inteiro)
+ Requer, além do objeto do agente que irá implementá-lo, um número inteiro que definirá quando o ParalellBehaviour deve finalizar. Podem
ser usadas as constantes descritas acima (WHEN\_ALL, WHEN\_ANY) ou um número inteiro.
    + Exemplos:  
```
new ParallelBihaviour(this, WHEN_ANY);
new ParallelBihaviour(this, WHEN_ALL);
new ParallelBihaviour(this, 3);
```  
No último exemplo, quando três subcomportamentos terminarem, o ParallelBehaviour será finalizado.  
+ Adiciona sub comportamentos com o método __addSubBehaviour(Behaviour)__, onde _Behaviour_ é um objeto da classe Behaviour ou filhas;

##### __3. FSMBehaviour__:
+ Baseia-se no funcionamento de uma máquina de estados finita (Finit State Machine);
+ Método __done()__ não é implementado;
+ Método __action()__ não é implementado;
+ Cada subcomportamento representa um estado;
+ Possui métodos específicos para registrar estados e transições:

| __Método__ | __Argumentos__ | __Retorno__ | __Descrição__ |
| ---------- | -------------- | ----------- | ------------- |
| registerFirstState | _Behaviour_, _String_ | _void_ | Registra um estado (subcomportamento) inicial. O FSMBehaviour registra um único subcomportamento como inicial. A String é o nome dado ao estado. |
| registerLastState | _Behaviour_, _String_ | _void_ | Registra um estado final. O FSMBehaviour registra vários subcomportamento como estados finais. A String é o nome dado ao estado. |
| registerState | _Behaviour_, _String_ | _void_ | Registra um subcomportamento como estado intermediário da máquina de estados. A String é o nome dado ao estado. |
| registerTransition | _String_^1, _String_^2, _Integer_ | _void_ | Define uma transição do estado _String_^1 para o estado _String_^2. O valor _Integer_ é um valor comparado ao retorno do método __done()__ do estado _String_^1. |
| registerDefaultTransition | _String_^1, _String_^2* | _void_ | Define uma transição padrão, isto é, ocorrerá independente do retorno do método __done()__ do estado _String_^1. |

*o argumento é facultativo