## Agente Binario

#### Finalidade
Converter números decimais passados como argumentos na inicialização da classe 
para números binários.
<br>

#### Uso
Com o JADE configurado corretamente no classpath do sistema, compile (usando 
_javac_) o arquivo __Binario.java__. Será criado o diretório _laai/examples_
no lugar onde o arquivo foi compilado (se não for utilizada a opção __-d__ do 
_javac_). Na pasta pai do diretório acima referido, utilize o comando
``` shell
java jade.Boot -gui -agents <nome_agente>:laai.examples.Binario(<argumentos>)
```
onde __\<nome_agente\>__ é um nome que será dado ao novo agente que está sendo 
criado e __\<argumentos\>__ são os números a serem convertidos para binário.
<br><br>
__Exemplo:__ 

``` shell
java jade.Boot -gui -agents bin:laai.examples.Binario(14, 13, 15, 1024)
``` 

+ __Observação:__ No Linux, utilize \ antes dos parêntesis para que o shell os 
reconheça corretamente.
<br>

#### Mapa da classe

+ __Atributos:__  
Nenhum.  

+ __Métodos:__

| Nome | Argumentos | Retorno | Observações |
| ---- | ---------- | ------- | ----------- |
| toBinary | __int__ _decimal_ | __ArrayList\<Integer\>__ | Converte um decimal em binário. |
| setup | - | __void__ | Implementado da classe _Agent_ |
| takeDown | - | __void__ | Sobrescrito da classe _Agent_ |
