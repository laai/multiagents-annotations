package laai.examples;

import jade.core.Agent;
import jade.core.AID;
import java.util.ArrayList;

public class Binario extends Agent {

	public static ArrayList<Integer> toBinary(int decimal) {
		ArrayList<Integer> binario = new ArrayList<Integer>();
		if(decimal == 0 || decimal == 1){
			binario.add(decimal);
			return binario;
		}
		else {
			ArrayList<Integer> parte = toBinary(decimal/2);
			binario.addAll(parte);
			binario.add(decimal%2);
			return binario;
		}
	}

	//Método invocado na inicialização do agente
	protected void setup() {
		System.out.println("Agente " + getAID().getLocalName() + ":Binario iniciado corretamente.");

		//Captura os argumentos passados com a inicialização do agente		
		Object[] argumentos = getArguments();

		//Verifica se existem argumentos para serem processados
		if (argumentos != null && argumentos.length > 0) {
			ArrayList<Integer> numeros = new ArrayList<Integer>();

			//Guarda todos os argumentos passados em uma lista de inteiros, lançando possíveis erros de conversão
			for (int i=0; i<argumentos.length; i++) {
				try {
					numeros.add(Integer.parseInt((String) argumentos[i]));
				}
				catch(Exception erro) {
					System.out.println("Problemas ao converter o argumento " + argumentos[i] + ".");
					System.out.println("Informações: " + erro.getMessage());
				}
			}

			//Converte e mostra todos os números passados como argumentos
			System.out.println("DECIMAL				BINÁRIO");
			for (int i=0; i<numeros.size(); i++) {
				System.out.println(numeros.get(i) + "				" + this.toBinary(numeros.get(i)));
			}
		}
		else
			System.out.println("Nenhum número para converter");
	}

	protected void takeDown() {
		System.out.println("Agente " + getAID().getLocalName() + ":Binario encerrado.");
	}
}
