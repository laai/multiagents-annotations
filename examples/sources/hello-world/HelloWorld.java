package laai.examples;

import jade.core.Agent;

public class HelloWorld extends Agent {
	//Método chamado na inicialização do Agente
	protected void setup() {
		System.out.println("Olá, mundo!");
		System.out.println("Meu nome é " + this.getLocalName());
		//getLocalName() é um método da classe Agent sobrescrito. Ele retorna uma string com o nome local do agente.
	}
}
