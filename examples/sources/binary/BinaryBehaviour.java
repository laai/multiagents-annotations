package laai.examples;

import jade.core.behaviours.CyclicBehaviour;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.core.AID;
import java.util.ArrayList;

public class BinaryBehaviour extends CyclicBehaviour {
	public BinaryBehaviour(Agent a) {
		super(a);
	}

	public void action() {
		// Recebe uma mensagem enviada ao agente
		ACLMessage mensagem = myAgent.receive();
		// Caso não haja mensagens na "caixa de entrada", o método Agent.receive() retorna NULL
		if(mensagem != null) {
			// Extrai o conteúdo da mensagem recebida
			String conteudo = mensagem.getContent();
			System.out.println("Mensagem recebida do agente " + mensagem.getSender().getName());
			// Cria uma resposta ao agente que enviou a requisição
			ACLMessage resposta = mensagem.createReply();
	
			try {
				int decimal = Integer.parseInt(conteudo);
				// Adiciona um ato comunicativo à mensagem (FIPA)
				resposta.setPerformative(ACLMessage.INFORM);
				// Adiciona um conteúdo à mensagem
				resposta.setContent(toBinary(decimal).toString());
				// Envia a mensagem de resposta
				myAgent.send(resposta);
				System.out.println("Resposta enviada ao agente " + mensagem.getSender().getName());
			}
			catch(Exception erro) {
				System.out.println("Problemas ao converter o argumento " + mensagem.getContent() + ".");
				resposta.setPerformative(ACLMessage.FAILURE);
				resposta.setContent("I could not convert " + mensagem.getContent() + " to binary");
				myAgent.send(resposta);
				System.out.println("Uma mensagem de erro foi enviada ao agente " + mensagem.getSender().getName());
			}
		}
		else
			// Bloquea o comportamento até o agente receber uma nova mensagem
			block();
	}

	public ArrayList<Integer> toBinary(int decimal) {
		ArrayList<Integer> binario = new ArrayList<Integer>();
		if(decimal == 0 || decimal == 1){
			binario.add(decimal);
			return binario;
		}
		else {
			ArrayList<Integer> parte = toBinary(decimal/2);
			binario.addAll(parte);
			binario.add(decimal%2);
			return binario;
		}
	}
}