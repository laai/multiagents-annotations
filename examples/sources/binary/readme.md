## Agente Binary

#### Finalidade
Receber números decimais, através de mensagens ACL-FIPA, e responder ao agente
requisitante, também por mensagem ACL, com o correspondente em binário.
<br>

#### Uso
Com o JADE configurado corretamente no classpath do sistema, compile o arquivo 
__Binary.java__ e o arquivo __BinaryBehaviour.java__. Será criado um diretório 
chamado _laai/examples_ no lugar onde o arquivo foi compilado (use a opção __-d__ 
do _javac_ para obter um local diferente). Na pasta pai do diretório acima 
referido, utilize o comando
``` shell
java jade.Boot -gui -agents <nome_agente>:laai.examples.Binary
```
onde __\<nome_agente\>__ é um nome que será dado ao novo agente que você está 
instanciando.
<br><br>
__Exemplo:__ 

``` shell
java jade.Boot -gui -agents bino:laai.examples.Binario
``` 
<br>

#### Mapa da classes

+ __Atributos:__  
Nenhum.  

+ __Métodos:__

| Classe | Nome | Argumentos | Retorno | Informações |
| ------ | ---- | ---------- | ------- | ----------- |
| BinaryBehaviour | BinaryBehaviour | __Agent__ _a_  | __void__ | Construtor da classe BinaryBehaviour. Recebe como argumento o agente que terá este comportamento. |
| BinaryBehaviour | action | - | __void__ | Especifica a ação do comportamento. |
| BinaryBehaviour | toBinary | __int__ _decimal_ | __ArrayList\<Integer\>__ | Converte um decimal em binário. |
| Binary | setup | __void__ | - | Implementado da classe _Agent_ |
| Binary | takeDown | __void__ | - | Sobrescrito da classe _Agent_ |
