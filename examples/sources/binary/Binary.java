package laai.examples;

import jade.core.Agent;
import jade.core.AID;

public class Binary extends Agent {
	protected void setup() {
		System.out.println("Agente " + getAID().getLocalName() + " iniciado corretamente.");

		// Adiciona um comportamento cíclico
		BinaryBehaviour beh = new BinaryBehaviour(this);
		addBehaviour(beh);
	}

	protected void takeDown() {
		System.out.println("O agente " + getAID().getLocalName() + " foi finalizado.");
	}
}