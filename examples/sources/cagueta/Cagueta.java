package laai.examples;

import jade.core.Agent;
import jade.core.AID;
import javax.swing.JOptionPane;
import java.util.Iterator;
import java.util.Scanner;

public class Cagueta extends Agent {
	//método invocado na inicialização do agente
	protected void setup() {
		//captura os endereços do agente
		String enderecos = "[";
		Iterator i = getAID().getAllAddresses();
		while(i.hasNext())
			enderecos += i.next() + " ";
		enderecos += "]";

		//Mostra as informações do agente na tela (JOptionPane)
		System.out.println("Oi, sou o agente " + getAID().getLocalName() + ". Mostrarei minhas informações em sua tela. Tecle ENTER para ver.");
		Scanner teclado = new Scanner(System.in);
		teclado.nextLine();
		JOptionPane.showMessageDialog(null,
				"Nome local: " + getAID().getLocalName() +
				"\nNome global (GUID): " + getAID().getName() +
				"\nEndereços: " + enderecos +
				"\nAID (FIPA): " + getAID(),
			getAID().getLocalName() + " informations",
			JOptionPane.INFORMATION_MESSAGE);
	}
}